# Getting started
This is a project to get started on the gitlab CI for compiling and testing code.

The master branch has a very rudimentary `.gitlab-ci.yml` file to build the project once.

You find the  branches `testlevel*`  with increasing test complexity in the script `.gitlab-ci.yml`.

Important things:
-  Explanations of the YAML syntax of the file **.gitlab-ci.yml** [found here](https://docs.gitlab.com/ce/ci/yaml)
- commits to not check: add `[CI skip]` in the commit message
- you can disable the email notifications under your personal notification settings in the gitlab


## Setup a local runner:
- Installation of a local runner [found here](https://docs.gitlab.com/runner/install/) be sure to install the version ~~<= 1.11.4~~   
  compatible with the mpcdf gitlab version (which is 9.1.4 since 15.05.2017), a compatibility chart can be 
  [found here](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/blob/master/README.md)  
  `sudo apt-get install gitlab-ci-multi-runner`  
  or, for installing a specific version, `sudo apt-get install gitlab-ci-multi-runner=1.11.4` 
- After installation of the runner, execute **in your home directory**  
  `gitlab-runner register`  
  follow the steps with the url and token given on the `Settings->CI/CD pipeline` of your project on the gitlab page
- then start the runner  
  `gitlab-runner run`  
  You should see the runner on the gitlab page  `Settings->CI/CD pipeline`

## Tags:
A tag or a list of tags allows to choose on which runner the job will be executed.

The use of tags is recommended, use at least one tag for the job, corresponding to the tag(s) of the runner. 

Note the following:
1. Each job is assigned to only **one** runner! If you want to execute the same job on another runner,  
   you have to copy the job and change the tag!
1. If job has no tag
    - one runner must be configured to run untagged jobs, else the job cannot be executed 
1. Job has tags
    - **all** tags of a job **must match a subset** of the tags of **one** runner

**Careful**, different runners should not have the same tag!

Each job can have one or multiple tags, here an example:
```
job_1:
  stage: build
  tags:
    - tag1_runnerA
job_2:
  stage: build
  tags:
    - tag1_runnerB
job_3:
  stage: test
  tags:
    - tag1_runnerA
    - tag2_runnerA
```
- runner A  has the tags `tag1_runnerA` and `tag2_runnerA` ... and therefore runs  `job_1` and `job_3`
- runner B  has the tags `tag1_runnerB` and `tag2_runnerB` ... and therefore runs only `job_2`
- **Careful**, the tag list of one job should contain only tags of **one** runner!
