! Runge-Kutta solution of the ODE
!
!   x' =    y
!   y' = -4*x
!
! with omega=2 and initial condition  x(t=0)=x0, y(t=0)=0 .
!
! The exact solution is 
!
!   x(t) =    x0 * cos(2*t)
!   y(t) = -2*x0 * sin(2*t)
!
! The RK method is defined for a general ODE
!
!   u' = f(u)
!
! as
!
!   w = u^n + 3/5*dt * f(u^n)
!   u^{n+1} = u^n + dt * f(w)
!
module mod_rk_ode

 use mod_kinds, only: wp

 implicit none

 public :: compute_rk_solution

 private

contains
 
 ! Solve the ODE from  t=0  to  tfinal=nsteps*dt
 pure subroutine compute_rk_solution(x,y , x0,dt,nsteps)
  real(wp), intent(in)  :: x0     ! initial condition
  real(wp), intent(in)  :: dt     ! time step
  integer,  intent(in)  :: nsteps ! number of steps; tfinal=nsteps*dt
  real(wp), intent(out) :: x      ! x(tfinal)
  real(wp), intent(out) :: y      ! y(tfinal)

  real(wp), parameter :: a = 3.0_wp/5.0_wp

  integer :: n
  real(wp) :: xnew, ynew ! x^{n+1}, y^{n+1}
  real(wp) :: wx, wy     ! intermediate stage w, components x,y

   ! initial condition
   x = x0
   y = 0.0_wp

   ! time loop
   do n=1,nsteps
     ! RK stage
     wx = x + a*dt *          y
     wy = y + a*dt * (-4.0_wp*x)
     ! solution update
     xnew = x + dt *          wy
     ynew = y + dt * (-4.0_wp*wx)
     ! redefine x,y
     x = xnew
     y = ynew
   enddo

 end subroutine compute_rk_solution

end module mod_rk_ode
