program rk_solve

 use mod_kinds, only: wp
 use mod_rk_ode, only: compute_rk_solution

 implicit none

 integer, parameter :: num_st = 10
 real(wp), parameter :: dt = 0.1_wp
 real(wp), parameter :: x_in = 1.0_wp

 real(wp) :: x,y

  call compute_rk_solution(x,y , x_in,dt,num_st)

  write(*,*) "After ",num_st," num_st with dt=",dt, &
       " the numerical solution is:  x=",x,", y=",y

  write(*,*) "The exact solution is: ", &
   "x=",         x_in*cos(2.0_wp * real(num_st,wp)*dt), &
   "y=", -2.0_wp*x_in*sin(2.0_wp * real(num_st,wp)*dt)

  write(*,*) "The error is: ", max( &
    abs(         x_in*cos(2.0_wp * real(num_st,wp)*dt) - x ) , &
    abs( -2.0_wp*x_in*sin(2.0_wp * real(num_st,wp)*dt) - y ) )

  write(*,*) " "
  write(*,*) "PROGRAM RK_SOLVE FINISHED SUCCESSFULLY."

end program rk_solve
